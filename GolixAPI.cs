﻿//using ServiceStack.Text;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Threading;


namespace GolixBot
{
    public class GolixAPI
    {
        private const string domain = "https://golix.com";
        private string apiKey = "";
        private string apiSecret = "";
        public string tonce = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();
        public string market = "";

        public GolixAPI(string apiKey, string apiSecret)
        {
            this.apiKey = apiKey;
            this.apiSecret = apiSecret;
        }
        #region Query

        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        private byte[] hmacsha256(byte[] keyByte, byte[] messageBytes)
        {
            using (var hash = new HMACSHA256(keyByte))
            {
                return hash.ComputeHash(messageBytes);
            }
        }

        private string BuildQueryData(Dictionary<string, string> param)
        {
            if (param == null)
                return "";

            StringBuilder b = new StringBuilder();
            StringBuilder c = new StringBuilder();
            
            foreach (var item in param)
                b.Append(string.Format("{0}={1}", item.Key, item.Value));
            foreach (var item in param)
                c.Append(string.Format("&{0}={1}", item.Key, item.Value));

            b.ToString();
            string res = b[0].Equals('?') ? b.ToString() : c.ToString();
           
            try { return res;  }
            catch (Exception) { return ""; }

        }

        private string Query(string method, string function, Dictionary<string, string> param = null, Dictionary<string, string> paraSec = null, bool auth = false, bool ext = false, 
            bool special = false, Dictionary<string, string> pSpecial = null)
        {
            
            string paramData = BuildQueryData(param);
            string paramSec = BuildQueryData(paraSec);
            string specialp = BuildQueryData(pSpecial);
            string url = "/api/v1/" + function;
            string access = "?access_key=" + apiKey + "&tonce=" + tonce;
            string payload = method + "|/api/v1/" + function + "|access_key=" + apiKey + "&tonce=" + tonce;
            string postData = "";
            bool meth = method == "POST";

            url = !meth ? url + paramData + paramSec : url;
            payload = ext ? method + "|/api/v1/" + function + "|access_key=" + apiKey + paramData + "&tonce=" + tonce : payload;
            payload = special ? method + "|/api/v1/" + function + "|access_key=" + apiKey + paramData + "&tonce=" + tonce + specialp : payload;

            byte[] signatureBytes = hmacsha256(Encoding.ASCII.GetBytes(apiSecret), Encoding.ASCII.GetBytes(payload));
            string signatureString = "&signature=" + ByteArrayToString(signatureBytes);

            url = auth && !meth ? "/api/v1/" + function + access + signatureString + paramData + paramSec : url;
        
            Console.WriteLine(payload);

            postData = meth ? access.Substring(1) + signatureString + paramData + specialp + paramSec  : "";
            Console.WriteLine(postData);

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(domain + url);
            webRequest.Method = method;      
      
            try
            {
                if (postData != "")
                {
                    var data = Encoding.ASCII.GetBytes(postData);
                    webRequest.ContentType = "application/x-www-form-urlencoded";
                    webRequest.ContentLength = data.Length;

                    using (var stream = webRequest.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
                }
                using (WebResponse webResponse = webRequest.GetResponse())
                using (Stream str = webResponse.GetResponseStream())
                using (StreamReader sr = new StreamReader(str))
                {
                    return sr.ReadToEnd();
                }
                
            }
            catch(WebException wex)
            {
                
                Console.WriteLine(wex);
                using (HttpWebResponse response = (HttpWebResponse)wex.Response)
                {
                    if (response == null)
                        throw;
                            
                    using(Stream str = response.GetResponseStream())
                    {
                        using(StreamReader sr = new StreamReader(str))
                        {
                            return sr.ReadToEnd();
                        }
                    }
                }
            }   
        }
        #endregion

        #region Get Methods
    
        public object GetTrades(string market)
        {
            var param = new Dictionary<string, string>();
            param["?market"] = market;
            string res = Query("GET","trades", param);
            string jsonstring = JsonConvert.SerializeObject(res);
           
            return res;
        }

        public object GetOrders(string market)
        {
            var param = new Dictionary<string, string>();
            var paramSec = new Dictionary<string, string>();
            paramSec = null;
            param["market"] = market;
            this.market = market;
            string res = Query("GET", "orders", param, paramSec, true , true);

            return res;
        }

        public object GetDeposits()
        {
            var param = new Dictionary<string, string>();
            var paramSec = new Dictionary<string, string>();
            param = null;
            paramSec = null;
            string res = Query("GET", "deposits", param, paramSec, true);

            return res;
        }

        public object GetWithdrawls()
        {
            var param = new Dictionary<string, string>();
            var paramSec = new Dictionary<string, string>();
            param = null;
            paramSec = null;
            string res = Query("GET", "withdrawals", param, paramSec, true);

            return res;
        }

        public object GetMarkets()
        {
            var param = new Dictionary<string, string>();
            var paramSec = new Dictionary<string, string>();
            param = null;
            paramSec = null;
            string res = Query("GET", "markets");

            return res;
        }

        public object GetTickers()
        {
            var param = new Dictionary<string, string>();
            var paramSec = new Dictionary<string, string>();
            param = null;
            paramSec = null;
            string res = Query("GET", "tickers");

            return res;
        }

        public object GetOrderBook(string market)
        {
            var param = new Dictionary<string, string>();
            var paramSec = new Dictionary<string, string>();
            paramSec = null;
            param["?market"] = market;

            string res = Query("GET", "order_book", param, paramSec);
            return res;
        }

        public object GetDepth(string market)
        {
            var param = new Dictionary<string, string>();
            var paramSec = new Dictionary<string, string>();
            paramSec = null;
            param["?market"] = market;

            string res = Query("GET", "depth", param, paramSec);
            return res;
        }

        #endregion

        #region Post Methods
        public string PostOrders(string Symbol, string Side, int Volume,double Price)
        {
            var param = new Dictionary<string, string>();
            var paramsec = new Dictionary<string, string>();
            var pSpecial = new Dictionary<string, string>();
            paramsec = null;
            
            param["market"] = Symbol;
            param["price"] = Price.ToString();
            param["side"] = Side;
          
           pSpecial["volume"] = Volume.ToString();
         //param["ord_type"] = "limit";

            return Query("POST", "orders", param, paramsec, true, true, true, pSpecial);
        }

        public string CancelAllOrders(string Side)
        {
            var param = new Dictionary<string, string>();
            var paramSec = new Dictionary<string, string>();

            param["side"] = Side;
            paramSec = null;

            return Query("POST", "orders/clear", param, paramSec, true, true);
        }


        public string PostDeposit(double Amount, string Currency, string From)
        {
            var param = new Dictionary<string, string>();
            var paramSec = new Dictionary<string, string>();

            param["amount"] = Amount.ToString();
            param["currency"] = Currency;
            param["from"] = From;

            paramSec = null;

            return Query("POST","deposit/funds", param, paramSec, true, true);
        }
        #endregion
    }

}
