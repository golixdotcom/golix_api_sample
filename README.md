# Golix_API_Connection

This is a sample API connection code.

The apikey and apisecret can be generated after you loggin to golix.com

Add your apikey and apisecret to Form1.cs file 

It is free to use and modify for your own strategy. This connection provides the following:

A GolixAPI Class that connects to Golix REST API
-All data is realtime via Websockets.
-Orders can be created, queried and cancelled 

A Form1 class that uses the Golix API class methods
Operations can be executed in this class and data is queried from the API classass

